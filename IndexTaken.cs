﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChaosArray
{
    class IndexTaken : Exception
    {
        //Creating static int to help ex.message explain where index error occured 
        public static int indexNumber { get; set; }
        //Constructor for creating exception with base method
        public IndexTaken(int id):base (String.Format($"Index [{id}] is taken.Try again."))
        {

        }
        
    }
}
