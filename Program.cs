﻿using System;

namespace ChaosArray
{
    class Program
    {
        static void Main(string[] args)
        {

            //Creating array of string
            CrazyCollection<String> StringArray = new CrazyCollection<string>();

            //Attempting to insert ten strings into array
            StringArray.Insert("first");
            StringArray.Insert("second");
            StringArray.Insert("third");
            StringArray.Insert("fourth");
            StringArray.Insert("fifth");
            StringArray.Insert("sixth");
            StringArray.Insert("seventh");
            StringArray.Insert("eight");
            StringArray.Insert("nine");
            StringArray.Insert("ten");

            //Viewing all elements to see how many actually got inserted
            StringArray.ViewAll();

            //Trying to retrive item ten times
            StringArray.Retrive();
            StringArray.Retrive();
            StringArray.Retrive();
            StringArray.Retrive();
            StringArray.Retrive();
            StringArray.Retrive();
            StringArray.Retrive();
            StringArray.Retrive();
            StringArray.Retrive();
        }
    }
}
