﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChaosArray
{
    class CrazyCollection<T>
    {
        private T[] Collection = new T[10];

        ////Method to retrive random generic item
        public void Retrive()
        {
            try
            {
                //Create random number from 0-9
                Random random = new Random();
                int randomIndex = random.Next(10);

                if (Collection[randomIndex] == null)
                {
                    EmptyIndex.indexNumber = randomIndex;
                    throw new EmptyIndex(randomIndex); //Throw custom exception if needed
                }
                else
                {
                    Console.WriteLine(Collection[randomIndex]);
                }
            }
            catch (EmptyIndex ex) //Catch custom exception
            {
                Console.WriteLine(ex.Message);
            }


        }
        //Method to insert generic item at random index
        public void Insert(T item)
        {
            try
            {
                Random random = new Random();
                int randomIndex = random.Next(10);
                if (Collection[randomIndex] == null)
                {
                    Collection[randomIndex] = item;          
                }
                else
                {
                    IndexTaken.indexNumber = randomIndex;
                    throw new IndexTaken(randomIndex);

                }
            }
            catch (IndexTaken ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        //Method to view full array
        public void ViewAll()
        {
            foreach (T item in Collection)
            {
                if (item == null)
                {
                    Console.WriteLine($"Found: Empty Space");
                }
                else
                {
                    Console.WriteLine($"Found: {item}");
                }
                
            }
        }
    }
}
